package day01;

public class day04_04 {
    public static void main(String[] args) {
        int num = 0;

        for(int i=1900;i<=2023;i++){
            boolean runY = (i % 4 == 0 && i % 100 != 0);
            if (runY) {
                System.out.print(i+" ");
                num++;
            }
            if (num==10){
                System.out.println();
                num = 0;
            }

        }
    }
}

