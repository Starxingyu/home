package day01;

import java.util.Scanner;

public class Program06 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("输入三角形的三条边");
        System.out.print("第一条边:");
        int a = scan.nextInt();
        System.out.print("第二条边:");
        int b = scan.nextInt();
        System.out.print("第三条边:");
        int c = scan.nextInt();
        if (a + b > c && a + c > b && b + c > a) {
            System.out.println("周长为:" + (a + b + c));
        } else {
            System.out.println("非法三角形");
        }

    }
}

