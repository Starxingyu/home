package day01;

import java.util.Scanner;

public class day04_02 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("年份：");
        int year = scan.nextInt();
        System.out.print("月份：");
        int mouth = scan.nextInt();

        boolean runY = (year % 4 == 0 && year % 100 != 0);

        int day = 0;

        int[] a = {1,3,5,7,8,10,12};
        int[] b = {4,6,9,11};
        if (mouth>0 && mouth<13){
            if (mouth==2){
                day = (runY?29:28);
            }
            for (int f: a) {
                if (f == mouth) {
                    day = 31;
                    break;
                }
            }
            for (int f: b) {
                if (f == mouth) {
                    day = 30;
                    break;
                }
            }
            System.out.println("有"+day+"天");




        }
    }

}
