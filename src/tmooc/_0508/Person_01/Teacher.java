package tmooc._0508.Person_01;

/**
 * 老师类
 * 原生类
 */
public class Teacher  extends Person{

    double salary;

    Teacher(String name, int age , String address,double salary){
        super(name, age, address);
        this.salary = salary;
    }
    void teach(){
        System.out.println(name + "正在讲课");
    }

    void asyHi() {
        System.out.println("大家好,我叫" + name + "今年" + age + "岁了,家住在" + address + "工资是:" + salary);
    }
}
