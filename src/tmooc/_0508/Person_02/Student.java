package tmooc._0508.Person_02;

public class Student extends Person {

    String classname;
    String stuId;

    Student(String name, int age, String address, String classname, String stuId) {
        super(name, age, address);
        this.classname = classname;
        this.stuId = stuId;
    }


    void asyHi() {
        System.out.println("大家好,我叫" + name + "今年" + age + "岁了,家住在" + address + "学号为," + stuId);
    }
}

