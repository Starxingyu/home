package tmooc._0508.Person_03;

/**
 * 超类
 */
public class Person {
    String name;
    int age;
    String address;

    Person(String name ,int age ,String address){
        this.address = address;
        this.age = age;
        this.name = name;
    }

    void eat() {
        System.out.println(name + "正在吃饭");
    }
    void sleep() {
        System.out.println(name + "正在睡觉");
    }
    void asyHi() {
        System.out.println("大家好,我叫" + name + "今年" + age + "岁了,家住在" + address);
    }

}
